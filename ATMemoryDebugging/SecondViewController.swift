//
//  SecondViewController.swift
//  ATMemoryDebugging
//
//  Created by Dejan on 14/11/2018.
//  Copyright © 2018 agostini.tech. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    var controller: MyController?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        controller = MyController("Test controller")
        controller?.delegate = self
    }
    
    deinit {
        print("Second controller deinit")
    }
}

extension SecondViewController: MyControllerDelegate {
    func someCallback() {
        // NoOP
    }
}

protocol MyControllerDelegate {
    func someCallback()
}

class MyController {
    
    let name: String
    var delegate: MyControllerDelegate?
    
    init(_ name: String) {
        self.name = name
    }
}

